SELECT customerName FROM customers
WHERE country = 'Philippines';

SELECT contactLastName, contactFirstName FROM customers
WHERE customerName = 'La Rochelle Gifts';

SELECT firstName, lastName FROM employees
WHERE email = 'jfirrelli@classicmodelcars.com';

SELECT customerName FROM customers
WHERE state IS NULL;

SELECT firstName, lastName, email FROM employees
WHERE lastName = 'Patterson' AND firstName = 'Steve';

SELECT customerName, country, creditLimit FROM customers
WHERE country != 'USA' AND creditLimit > 3000;

SELECT customerNumber FROM orders
WHERE comments LIKE '%DHL%';

SELECT productLine FROM productlines
WHERE textDescription LIKE '%state of the art%';

SELECT DISTINCT country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName, country FROM customers
WHERE country IN ('USA', 'France', 'Canada');

SELECT 
    employees.firstName, 
    employees.lastName, 
    offices.city 
FROM 
    employees 
    INNER JOIN offices ON employees.officeCode = offices.officeCode
WHERE 
    offices.city = 'Tokyo';

SELECT customerName FROM customers
WHERE customerNumber IN 
(SELECT customerNumber FROM orders WHERE salesRepEmployeeNumber = 
(SELECT employeeNumber FROM employees WHERE lastName = 'Thompson' AND firstName = 'Leslie'));


SELECT productName, customerName FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber
WHERE customers.customerName = 'Baane Mini Imports';

SELECT 
    employees.firstName, 
    employees.lastName, 
    customers.customerName, 
    offices.country 
FROM 
    employees 
    INNER JOIN offices ON employees.officeCode = offices.officeCode 
    INNER JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
WHERE 
    offices.country = customers.country;


SELECT 
    products.productName, 
    products.quantityInStock 
FROM 
    products 
    INNER JOIN productlines ON products.productLine = productlines.productLine 
WHERE 
    productlines.productLine = 'planes' 
    AND products.quantityInStock < 1000;

SELECT 
    customerName 
FROM 
    customers 
WHERE 
    phone LIKE '%+81%';
